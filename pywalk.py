import os,stat,time,sys
for root, dirs, files in os.walk(sys.argv[1]):
    for filess in files:
        total_file = root+"\\"+filess
        accessTimesinceEpoc = os.path.getatime(total_file)
        accessTime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(accessTimesinceEpoc))
        file_name, file_extension = os.path.splitext(total_file)
        print(root+"\\"+'\t'+filess+'\t'+file_extension+"\t"+str(accessTime))
